import {colorMark, validateFields} from './functions.js'
import {Modal} from "./helpers.js";
import { Request } from "./request.js";
import {visitsContainer, visitsList} from "./variables.js";

export class visitCard {
  constructor(patientName, doctor, title, description, urgency, id, status = 'open') {
    this.patientName = patientName;
    this.doctor = doctor;
    this.id = id;
    this.title = title;
    this.description = description;
    this.urgency = urgency;
    this.status = status;
  }
  createCardInfo() {
    return `<div class="card-item__info-block info-block">
        <div class="card-item__info-row info-row">
            <span class="info-row__info-type">Visit purpose: </span>
            <span class="info-row__info-text">${this.title}</span>
        </div>
        <div class="card-item__info-row info-row">
            <span class="info-row__info-type">Patient: </span>
            <span class="info-row__info-text">${this.patientName}</span>
        </div>
            <div class="card-item__info-row info-row">
                <span class="info-row__info-type">Doctor: </span>
                <span class="info-row__info-text">${this.doctor.slice(0,1).toUpperCase()}${this.doctor.slice(1)}</span>
            </div>
        </div>
        <div class="card-item__control-block control-block">
            <div class="control-block__row">
                <p class="control-block__btn">
                    <i class="fa-solid fa-user-pen edit-btn"></i>
                </p>
                <p class="control-block__btn">
                    <i class="fa-solid fa-trash delete-btn"></i>
                </p>
            </div>
            <p class="control-block__row">
                <button> Show more...</button>
            </p>
        </div>`
  }
  createHTMLCard() {
    const cardItem = document.createElement('div');
    cardItem.classList.add('card-item');
    cardItem.dataset.id = this.id;
    cardItem.insertAdjacentHTML('beforeend', this.createCardInfo());
    return cardItem;
  }

  deleteBtnInit(currentTarget) {
    const controlPanel = currentTarget.querySelector('.control-block');
    const deleteBtn = controlPanel.querySelector('.delete-btn');

    deleteBtn.addEventListener('click', evt => {
      Request.deleteCard(this.id).then(data => {
        currentTarget.remove()
        if(!visitsList.childElementCount) {
          visitsContainer.querySelector('.container').remove();
          visitsContainer.insertAdjacentHTML('beforeend', `<div class="no-items">No visits found</div>`);
        }
      })
    })
  }

  generateEditHTMLFields() {
    return `<p>
                <label for="patient">Patient</label>
                <input placeholder='Name Lastname' class = 'input-base' type="text" name="patientName" value="${this.patientName}">
                </p>
                <p>
                    <label for="visit-urgency-edit ">Urgency</label>
                    <select class = 'visit-urgency-edit' name="urgency">
                    <option value="high">High</option>
                    <option value="normal">Normal</option>
                    <option value="low">Low</option>
                    </select>
                </p>
                 <p>
                    <label for="status">Visit status</label>
                    <select class = 'visit-urgency-edit' name="status">
                    <option value="open">Open</option>
                    <option value="done">Done</option>
                    </select>
                </p>
                <p>
                    <label for="title">Visit title</label>
                    <input class = 'input-base' type="text" name="title" value="${this.title}">
                </p>
                <p>
                    <label for="description">Visit description</label>
                    <input class = 'input-base' type="text" name="description" value="${this.description}">
                </p>`

  }

  generateAdvancedHTMLInfo() {
    return `<p><span>Patient: </span>${this.patientName}</p>
                <p><span>Doctor: </span>${this.doctor}</p>
                <p><span>Urgency: </span>${this.urgency}</p>
                <p><span>Status: </span>${this.status}</p>
                <p><span>Visit title: </span>${this.title}</p>
                <p><span>Visit description: </span>${this.description}</p>`
  }

  cardInteractionsInit(currentTarget) {
    const controlPanel = currentTarget.querySelector('.control-block');
    const editBtn = controlPanel.querySelector('.edit-btn');
    const showMoreBtn = controlPanel.querySelector('.control-block__row button')

    editBtn.addEventListener('click', evt => {
      const modalWrapper = new Modal('Edit visit information', this.generateEditHTMLFields(), 'modal-visit-info', 'module-container');
      modalWrapper.createModal();

      const confirmBtn = document.querySelector('.edit-form-btn');

      confirmBtn.addEventListener('click', evt => {
        const inputValuesArr = [...evt.currentTarget.closest('.modal-content').querySelectorAll('input')];
        const valuesFromInputs = [];
        inputValuesArr.forEach(input => valuesFromInputs.push(input.value));
        const selectFields = evt.currentTarget.closest('.modal-content').querySelectorAll('select');
        selectFields.forEach(field => valuesFromInputs.push(field.value))
        const valuesToSend = this.getAllInfo(valuesFromInputs);
        valuesToSend.doctor = this.doctor;
        for (let key in valuesToSend) {
          if(!validateFields(key, valuesToSend[key])) {
            const uncorrectedValue = document.querySelector(`input[name='${key}']`) || document.querySelector(`select[name='${key}']`);
            uncorrectedValue.style.borderColor = 'red';
            return;
          }
        }
        Request.editCard(this.id, valuesToSend)
          .then(data => {
            for (let key in this) {
              if (data[key] === undefined) continue;
              this[key] = data[key]
            }
            const newCardInfo = this.createCardInfo();
            const currentCard = document.querySelector(`[data-id="${this.id}"]`);
            currentCard.innerHTML = '';
            currentCard.insertAdjacentHTML('beforeend', newCardInfo);
            this.deleteBtnInit(currentCard);
            this.cardInteractionsInit(currentCard);
            colorMark(currentCard, data.urgency, data.status)
            document.querySelector('.modal-wrapper').remove()
          })
      })
    })

    showMoreBtn.addEventListener('click', evt => {
      const modalWrapper = new Modal('Visit information', this.generateAdvancedHTMLInfo(), 'modal-visit-info', 'module-container');
      modalWrapper.createModal();
    })
  }
}

export class visitCardCardiologist extends visitCard {
  constructor(patientName, doctor, title, description, urgency, id, status, arterialPressure, bmi, illnesses, age) {
    super(patientName, doctor, title, description, urgency, id, status);
    this.arterialPressure = arterialPressure;
    this.bmi = bmi;
    this.illnesses = illnesses;
    this.age = age;
  }

  generateAdvancedHTMLInfo() {
    return `${super.generateAdvancedHTMLInfo()}
                <p><span>Age: </span>${this.age}</p>
                <p><span>Arterial Pressure: </span>${this.arterialPressure}</p>
                <p><span>BMI: </span>${this.bmi}</p>
                <p><span>Ilnesses: </span>${this.illnesses}</p>`
  }

  generateEditHTMLFields() {
    return `${super.generateEditHTMLFields()}
                <p>
                    <label for="age">Age</label>
                    <input placeholder='More than 18' class = 'input-base' type="number" name="age" value="${this.age}">
                </p>
                <p>
                    <label class="label-visit" for="arterial-pressure">Arterial Pressure</label>
                    <input placeholder='120/80' class = 'input-base' type="text" name="arterialPressure" value="${this.arterialPressure}">
                </p>
                <p>
                    <label for="bmi">BMI</label>
                    <input placeholder='Weigth / height(metres)' class = 'input-base' type="text" name="bmi" value="${this.bmi}">
                </p>
                <p>
                    <label for="illnesses">Illnesses</label>
                    <input class = 'input-base' type="text" name="illnesses" value="${this.illnesses}">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn' >Confirm changes</button></div>`
  }

  getAllInfo(valuesArr) {
    const keysArr = ['patientName', 'title', 'description', 'age', 'arterialPressure', 'bmi', 'illnesses', 'urgency', 'status'];
    const objInfo = {};
    for (let [i, key] of keysArr.entries()) {
      objInfo[key] = valuesArr[i];
    }
    return objInfo;
  }
}

export class visitCardTherapist extends visitCard {
  constructor(patientName, doctor, title, description, urgency, id, status, age) {
    super(patientName, doctor, title, description, urgency, id, status);
    this.age = age;
  }

  generateAdvancedHTMLInfo() {
    return `${super.generateAdvancedHTMLInfo()}
                <p><span>Age: </span>${this.age}</p>`
  }

  generateEditHTMLFields() {
    return `${super.generateEditHTMLFields()}
                 <p>
                    <label for="age">Age</label>
                    <input placeholder='More than 18' class = 'input-base' type="number" name="age" value="${this.age}">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn'>Confirm changes</button></div>`
  }


  getAllInfo(valuesArr) {
    const keysArr = ['patientName', 'title', 'description', 'age', 'urgency', 'status'];
    const objInfo = {};
    for (let [i, key] of keysArr.entries()) {
      objInfo[key] = valuesArr[i];
    }
    return objInfo;
  }
}

export class visitCardDentist extends visitCard {
  constructor(patientName, doctor, title, description, urgency, id, status, lastVisitDate) {
    super(patientName, doctor, title, description, urgency, id, status);
    this.lastVisitDate = lastVisitDate;
  }

  generateAdvancedHTMLInfo() {
    return `${super.generateAdvancedHTMLInfo()}
                <p><span>Last Visit: </span>${this.lastVisitDate}</p>`
  }

  generateEditHTMLFields() {
    return `${super.generateEditHTMLFields()}
                <p>
                    <label for="last-visit">Last Visit</label>
                    <input max="${new Date().toISOString().split('T')[0]}" class = 'input-base' type="date" name="lastVisitDate" value="${this.lastVisitDate}">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn' >Confirm changes</button></div>`
  }

  getAllInfo(valuesArr) {
    const keysArr = ['patientName', 'title', 'description', 'lastVisitDate', 'urgency', 'status'];
    const objInfo = {};
    for (let [i, key] of keysArr.entries()) {
      objInfo[key] = valuesArr[i];
    }
    return objInfo;
  }
}