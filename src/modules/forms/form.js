import { Button } from "../button.js"
import { Select } from "./select.js"
import { Input } from "./input.js"

export class Form {
    static createElement(obj, form) {
        switch (obj.tagName) {
            case 'select': {
                const baseSelect = new Select('select', obj);
                const baseSelectHtml = baseSelect.creaeteSelect();
                if (obj.name === 'doctor') {
                    form.append(baseSelectHtml);
                } else {
                    form.insertBefore(baseSelectHtml, form.lastChild);
                }
                break;
            }
            case 'input': {
                const baseInput = new Input('input', obj);
                const baseInputHtml = baseInput.createInput();
                form.insertBefore(baseInputHtml, form.lastChild);
                break;
            }
            case 'button': {
                const baseButton = new Button('button', obj);
                const baseButtonHtml = baseButton.createButton();
                form.append(baseButtonHtml);
                break;
            }
        }
    }

    static validateFormInputs(form) {
        let isValid = true;
        const inputs = form.querySelectorAll('input');
        const inputValidations = {
            age: [/^(1[8-9]|[2-9]\d)$/, 'Enter correct age'],
            patientName: [/^[a-zA-Z]+\s[a-zA-Z]+$/, 'Enter first name and last name'],
            arterialPressure: [/^\d{2,3}\/\d{2,3}$/, 'Enter your pressure like 120/80'],
            bmi: [/^\d+(\.\d+)?\/\d+(\.\d+)?$/, 'Enter like example: 64/1.71']
        };
        inputs.forEach(input => {
            const inputValue = input.value;
            const inputErr = input.nextElementSibling;
            const validation = inputValidations[input.dataset.name];
            if (validation && !validation[0].test(inputValue)) {
                input.style.color = 'red';
                inputErr.classList.remove('hidden');
                inputErr.textContent = validation[1];
                isValid = false;
            } else {
                input.style.color = 'black';
                inputErr.classList.add('hidden');
            }
        })
        return isValid
    }
}