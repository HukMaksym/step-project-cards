import { ElementForm } from "./elementForm.js"

export class Select extends ElementForm {
    constructor( tagName, { name, title, options, className = 'select-base', required = 'required' }){
        super(...arguments);
        this.name = name;
        this.title = title;
        this.options = options;
        this.className = className;
        this.required = required;
    }

    creaeteSelect() {
        this.select = document.createElement(`${this.tagName}`);
        this.select.classList.add(`${this.className}`);
        this.select.classList.add('select-value');
        this.select.dataset.name = this.name;
        this.select.required = this.required;
        this.createOptions();
        return this.createSelectDescr();

    }

    createOptions() {
        const firstOption = document.createElement('option');
        firstOption.value = '';
        firstOption.textContent = this.options[0];
        firstOption.disabled = true;
        firstOption.selected = true;
        const descrOption = this.options.shift();
        const fragment = document.createDocumentFragment();
        fragment.appendChild(firstOption);
        this.options.forEach(value => {
            const option = document.createElement('option');
            option.value = value;
            option.textContent = value;
            fragment.appendChild(option);
        });
        this.options.unshift(descrOption);
        this.select.appendChild(fragment);
    }

    createSelectDescr() {
        const selectDescr = document.createElement('div');
        selectDescr.textContent = this.title;
        selectDescr.append(this.select);
        return selectDescr;
    }

}