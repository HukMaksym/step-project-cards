import { ElementForm } from "./elementForm.js"

export class Input extends ElementForm {
    constructor(tagName, { name, type, placeholder, label, className = 'input-base', required = 'required', dataMax = '', dataMin = '1900-01-01' }){
        super(...arguments);
        this.name = name;
        this.type = type;
        this.placeholder = placeholder;
        this.label = label;
        this.className = className;
        this.dataMax = dataMax;
        this.dataMin = dataMin;
        this.required = required;
    }

    createInput() {
        this.input = document.createElement(`${this.tagName}`);
        this.input.type = this.type;
        this.input.dataset.name = this.name;
        this.input.placeholder = this.placeholder;
        this.input.classList.add(`${this.className}`);
        this.input.required = this.required;
        if (this.name === 'lastVisitDate') {
            this.input.max = this.dateNow();
            this.input.min = this.dataMin;
            this.input.style.minWidth = '1%'
        }
        return this.putInputInLabel();
    }

    putInputInLabel() {
        const label = document.createElement('label');
        label.textContent = this.label;
        label.classList.add('label-visit');
        label.classList.add('base');
        label.append(this.input);
        const inputErr = document.createElement('div');
        inputErr.classList.add('input-error', 'hidden');
        label.insertAdjacentElement('beforeend', inputErr);
        return label;
    }

    dateNow() {
        const now = new Date();
        let day = now.getDate().toString().padStart(2, '0');
        let month = (now.getMonth() + 1).toString().padStart(2, '0');
        const year = now.getFullYear();
        return `${year}-${month}-${day}`;
    }
}