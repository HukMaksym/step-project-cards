import {ElementForm} from "./forms/elementForm.js";

export class Button extends ElementForm {
    constructor( tagName, { name, type, textContent, className = 'btn-submit' }){
        super(...arguments);
        this.name = name;
        this.type = type;
        this.textContent = textContent;
        this.className = className;
    }
    createButton() {
        this.button = document.createElement(`${this.tagName}`);
        this.button.textContent = this.textContent;
        this.button.type = this.type;
        this.button.classList.add(`${this.className}`);
        return this.button
    }
}