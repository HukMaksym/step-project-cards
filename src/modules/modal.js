const mainHtml = document.querySelector('main')

export class Modal {
    constructor(title, content, classNameCssSpecial = '', classNameCssBase = 'module-container'){
        this.title = title;
        this.content = content;
        this.classNameCssBase = classNameCssBase;
        this.classNameCssSpecial = classNameCssSpecial

    }
    createModal(){
        this.modal = document.createElement('div');
        this.modal.classList.add('modal-wrapper')
        this.modal.insertAdjacentHTML('beforeend', `<div class='${this.classNameCssBase} ${this.classNameCssSpecial}'>
            <h5 class="modal-title">${this.title}</h5>
            <div class="modal-content"></div>
            <div class="btn-close"></div>
        </div>`)
        const modalContent = this.modal.querySelector('.modal-content');
        if(typeof(this.content) === 'string') {
            modalContent.insertAdjacentHTML('beforeend', `${this.content}`)
        } else {
            modalContent.insertAdjacentElement('beforeend', this.content)
        }
        this.closeModal();
        mainHtml.insertAdjacentElement('beforeend', this.modal)
    }

    closeModal(){
        this.modal.addEventListener('click', ev => {
            const itemClose = ev.target.closest('.btn-close')
            if(itemClose) this.modal.remove()
        })
        this.modal.addEventListener('click', ev=> {
            if(ev.target.classList.contains('modal-wrapper')) this.modal.remove()
           
        })
    }
}