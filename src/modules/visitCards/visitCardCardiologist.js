import {visitCard} from "./visitCard.js";

export class visitCardCardiologist extends visitCard {
    constructor(patientName, doctor, title, description, urgency, id, status, arterialPressure, bmi, illnesses, age) {
        super(patientName, doctor, title, description, urgency, id, status);
        this.arterialPressure = arterialPressure;
        this.bmi = bmi;
        this.illnesses = illnesses;
        this.age = age;
    }

    generateAdvancedHTMLInfo() {
        return `${super.generateAdvancedHTMLInfo()}
                <p><span>Age: </span>${this.age}</p>
                <p><span>Arterial Pressure: </span>${this.arterialPressure}</p>
                <p><span>BMI: </span>${this.bmi}</p>
                <p><span>Ilnesses: </span>${this.illnesses}</p>`
    }

    getAllInfo(valuesArr) {
        const keysArr = ['patientName', 'doctor', 'title', 'description', 'age', 'arterialPressure', 'bmi', 'illnesses', 'urgency', 'status'];
        const objInfo = {};
        for (let [i, key] of keysArr.entries()) {
            objInfo[key] = valuesArr[i];
        }
        return objInfo;
    }
}