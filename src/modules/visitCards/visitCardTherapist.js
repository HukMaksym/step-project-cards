import {visitCard} from "./visitCard.js";

export class visitCardTherapist extends visitCard {
    constructor(patientName, doctor, title, description, urgency, id, status, age) {
        super(patientName, doctor, title, description, urgency, id, status);
        this.age = age;
    }

    generateAdvancedHTMLInfo() {
        return `${super.generateAdvancedHTMLInfo()}
                <p><span>Age: </span>${this.age}</p>`
    }


    getAllInfo(valuesArr) {
        const keysArr = ['patientName', 'doctor', 'title', 'description', 'age', 'urgency', 'status'];
        const objInfo = {};
        for (let [i, key] of keysArr.entries()) {
            objInfo[key] = valuesArr[i];
        }
        return objInfo;
    }
}