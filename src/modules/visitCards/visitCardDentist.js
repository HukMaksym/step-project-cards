import {visitCard} from "./visitCard.js";

export class visitCardDentist extends visitCard {
    constructor(patientName, doctor, title, description, urgency, id, status, lastVisitDate) {
        super(patientName, doctor, title, description, urgency, id, status);
        this.lastVisitDate = lastVisitDate;
    }

    generateAdvancedHTMLInfo() {
        return `${super.generateAdvancedHTMLInfo()}
                <p><span>Last Visit: </span>${this.lastVisitDate}</p>`
    }


    getAllInfo(valuesArr) {
        const keysArr = ['patientName', 'doctor', 'title', 'description', 'lastVisitDate', 'urgency', 'status'];
        const objInfo = {};
        for (let [i, key] of keysArr.entries()) {
            objInfo[key] = valuesArr[i];
        }
        return objInfo;
    }
}