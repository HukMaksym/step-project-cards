import { colorMark, validateFields } from "../functions.js";
import { Modal } from "../modal.js";
import { Request } from "../request.js";
import { visitsContainer, visitsList } from "../variables.js";
import { changeEditHTMLFields, cardRender } from "../functions.js";

export class visitCard {
  illnesses;
  constructor(
    patientName,
    doctor,
    title,
    description,
    urgency,
    id,
    status = "open"
  ) {
    this.patientName = patientName;
    this.doctor = doctor;
    this.id = id;
    this.title = title;
    this.description = description;
    this.urgency = urgency;
    this.status = status;
  }

  createCardInfo(
    patientName = this.patientName,
    title = this.title,
    doctor = this.doctor
  ) {
    return `<div class="card-item__info-block info-block">
        <div class="card-item__info-row info-row">
            <span class="info-row__info-type">Visit purpose: </span>
            <span class="info-row__info-text">${title}</span>
        </div>
        <div class="card-item__info-row info-row">
            <span class="info-row__info-type">Patient: </span>
            <span class="info-row__info-text">${patientName}</span>
        </div>
            <div class="card-item__info-row info-row">
                <span class="info-row__info-type">Doctor: </span>
                <span class="info-row__info-text">${doctor
                  .slice(0, 1)
                  .toUpperCase()}${this.doctor.slice(1)}</span>
            </div>
        </div>
        <div class="card-item__control-block control-block">
            <div class="control-block__row">
                <p class="control-block__btn">
                    <i class="fa-solid fa-user-pen edit-btn"></i>
                </p>
                <p class="control-block__btn">
                    <i class="fa-solid fa-trash delete-btn"></i>
                </p>
            </div>
            <p class="control-block__row">
                <button> Show more...</button>
            </p>
        </div>`;
  }

  createHTMLCard() {
    const cardItem = document.createElement("div");
    cardItem.classList.add("card-item");
    cardItem.dataset.id = this.id;
    cardItem.insertAdjacentHTML("beforeend", this.createCardInfo());
    return cardItem;
  }

  deleteBtnInit(currentTarget) {
    const controlPanel = currentTarget.querySelector(".control-block");
    const deleteBtn = controlPanel.querySelector(".delete-btn");

    deleteBtn.addEventListener("click", (evt) => {
      const content = `<div class="btn-wrapper delete-confirm"><button class = 'edit-form-btn confirm'>Delete visit</button><button class = 'edit-form-btn cancel'>Cancel</button></div>`;
      const modalWrapper = new Modal(
        "Do you want to delete visit?",
        content,
        "modal-visit-info",
        "module-container"
      );

      modalWrapper.createModal();

      document
        .querySelector(".edit-form-btn.confirm")
        .addEventListener("click", (e) => {
          Request.deleteCard(this.id).then((data) => {
            deleteBtn.closest(".card-item").remove();
            document.querySelector(".modal-wrapper").remove();
            if (!visitsList.childElementCount) {
              visitsContainer.querySelector(".container").remove();
              visitsContainer.insertAdjacentHTML(
                "beforeend",
                `<div class="no-items">No visits found</div>`
              );
            }
          });
        });

      document
        .querySelector(".edit-form-btn.cancel")
        .addEventListener("click", (evt) => {
          document.querySelector(".modal-wrapper").remove();
        });
    });
  }

  generateBasicEditField() {
    return `<div><p>
                <label for="patient">Patient</label>
                <input placeholder='Name Lastname' class = 'input-base' type="text" name="patientName" value="${this.patientName}">
                </p>
                 <p>
                    <label for="doctor">Doctor</label>
                    <select class = 'doctor-edit' name="doctor">
                    <option value="cardiologist">Cargiologist</option>
                    <option value="therapist">Therapist</option>
                    <option value="dentist">Dentist</option>
                    </select>
                </p>
                <p>
                    <label for="visit-urgency-edit ">Urgency</label>
                    <select class = 'visit-urgency-edit' name="urgency">
                    <option value="high">High</option>
                    <option value="normal">Normal</option>
                    <option value="low">Low</option>
                    </select>
                </p>
                 <p>
                    <label for="status">Visit status</label>
                    <select class = 'visit-urgency-edit' name="status">
                    <option value="open">Open</option>
                    <option value="done">Done</option>
                    </select>
                </p>
                <p>
                    <label for="title">Visit title</label>
                    <input class = 'input-base' type="text" name="title" value="${this.title}">
                </p>
                <p>
                    <label for="description">Visit description</label>
                    <input class = 'input-base' type="text" name="description" value="${this.description}">
                </p></div>`;
  }

  generateAdvancedEditField(doctor) {
    switch (doctor) {
      case "cardiologist":
        return `<div>
                <p>
                    <label for="age">Age</label>
                    <input placeholder='More than 18' value="${
                      this.age ?? ""
                    }" class = 'input-base' type="number" name="age">
                </p>
                <p>
                    <label class="label-visit" for="arterial-pressure">Arterial Pressure</label>
                    <input placeholder='120/80' value="${
                      this.arterialPressure ?? ""
                    }" class = 'input-base' type="text" name="arterialPressure">
                </p>
                <p>
                    <label for="bmi">BMI</label>
                    <input placeholder='Weight / height(metres)' value="${
                      this.bmi ?? ""
                    }" class = 'input-base' type="text" name="bmi">
                </p>
                <p>
                    <label for="illnesses">Illnesses</label>
                    <input placeholder="Serious illnesses you had" value="${
                      this.illnesses ?? ""
                    }" class = 'input-base' type="text" name="illnesses">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn' >Confirm changes</button></div>
                </div>`;
      case "therapist":
        return `<div>
                 <p>
                    <label for="age">Age</label>
                    <input placeholder='More than 18' value="${
                      this.age ?? ""
                    }" class = 'input-base' type="number" name="age">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn'>Confirm changes</button></div></div>`;
      case "dentist":
        return `<div><p>
                    <label for="last-visit">Last Visit</label>
                    <input max="${
                      new Date().toISOString().split("T")[0]
                    }" class = 'input-base' value="${
          this.lastVisitDate ?? ""
        }" type="date" name="lastVisitDate">
                </p>
                <div class="btn-wrapper"><button class = 'edit-form-btn' >Confirm changes</button></div></div>`;
      default:
        return false;
    }
  }

  generateAdvancedHTMLInfo() {
    return `<p><span>Patient: </span>${this.patientName}</p>
                <p><span>Doctor: </span>${this.doctor}</p>
                <p><span>Urgency: </span>${this.urgency}</p>
                <p><span>Status: </span>${this.status}</p>
                <p><span>Visit title: </span>${this.title}</p>
                <p><span>Visit description: </span>${this.description}</p>`;
  }

  confirmBtnListener = () => {
    const modalContent = document.querySelector(".modal-content");
    const newVisitData = {};
    [...modalContent.children].forEach((section, idx) => {
      [...section.querySelectorAll("p")].forEach((fieldWrapper) => {
        const field =
          fieldWrapper.querySelector("input") ??
          fieldWrapper.querySelector("select");
        newVisitData[field.name] = field.value;
      });
    });
    let flag = true;
    for (let key in newVisitData) {
      const field =
        document.querySelector(`input[name='${key}']`) ||
        document.querySelector(`select[name='${key}']`);
      if (!validateFields(key, newVisitData[key])) {
        const uncorrectedValue = (field.style.borderColor = "red");
        return;
      } else {
        field.style.borderColor = "";
      }
    }
    newVisitData.id = this.id;
    Request.editCard(this.id, newVisitData).then((data) => {
      cardRender(newVisitData);
      const cardDuplicates = [
        ...document.querySelectorAll(`[data-id="${this.id}"]`),
      ];
      const newHtmlCard = cardDuplicates[1];
      cardDuplicates[1].remove();
      visitsList.insertBefore(newHtmlCard, cardDuplicates[0]);
      cardDuplicates[0].remove();
      document.querySelector(".modal-wrapper").remove();
    });
  };

  cardInteractionsInit(currentTarget) {
    const controlPanel = currentTarget.querySelector(".control-block");
    const editBtn = controlPanel.querySelector(".edit-btn");
    const showMoreBtn = controlPanel.querySelector(
      ".control-block__row button"
    );

    editBtn.addEventListener("click", (evt) => {
      const content = `${this.generateBasicEditField()}${this.generateAdvancedEditField(
        this.doctor
      )}`;
      const modalWrapper = new Modal(
        "Edit visit information",
        content,
        "modal-edit-info",
        "module-container"
      );
      modalWrapper.createModal();
      let confirmBtn = document.querySelector(".edit-form-btn");

      const doctorSelect = document.querySelector(".doctor-edit");
      if (this.doctor === "cardiologist") {
        doctorSelect.options[0].setAttribute("selected", "selected");
      } else if (this.doctor === "therapist") {
        doctorSelect.options[1].setAttribute("selected", "selected");
      } else if (this.doctor === "dentist") {
        doctorSelect.options[2].setAttribute("selected", "selected");
      }
      const modalContent = document.querySelector(".modal-content");
      const fieldArr = [];
      document
        .querySelector(".doctor-edit")
        .addEventListener("change", (event) => {
          [...modalContent.children].forEach((field, idx) => {
            if (idx > 0) {
              field.remove();
            }
          });
          modalContent.insertAdjacentHTML(
            "beforeend",
            `${this.generateAdvancedEditField(event.currentTarget.value)}`
          );
          confirmBtn = document.querySelector(".edit-form-btn");
          confirmBtn.addEventListener("click", this.confirmBtnListener);
        });
      confirmBtn.addEventListener("click", this.confirmBtnListener);
    });

    showMoreBtn.addEventListener("click", (evt) => {
      const modalWrapper = new Modal(
        "Visit information",
        this.generateAdvancedHTMLInfo(),
        "modal-visit-info",
        "module-container"
      );
      modalWrapper.createModal();
    });
  }
}
